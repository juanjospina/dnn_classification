# Iris Dataset example
# This example demonstrates the creation and use of a neural network using
# Tensorflow.

# Author: Juan Ospina


# Import Libraries
from __future__ import print_function
import numpy as np
import pandas as pd
import tensorflow as tf
from sklearn.model_selection import train_test_split

# ----- Extract Process Training data -----
dataset_all = pd.read_csv('Data/iris.csv')
print('Shape of the train data with all features: ', dataset_all.shape)
# train = train.select_dtypes(exclude=['object']) # Exclude all non-numerical columns
dataset_all.fillna(0,inplace=True) # Fills the NaN spaces with Zeros

# Drop unrequired columns from dataset
# dataset_all = dataset_all.drop('sepal_l', 1)

col_all = list(dataset_all.columns)     # list of all columns
col_nolabel = list(dataset_all.columns) # list of all columns except labels
col_nolabel.remove('flower')            # Remove target(label) column

COLUMNS = col_all       # All Columns
FEATURES = col_nolabel  # Features (columns)
LABEL = "flower"        # Label (column) name

# features set and prediction set with features to predict
features_set = dataset_all[COLUMNS]
prediction_set = dataset_all.flower

# Do one-hot encoding on the labels (target column)
features_set = pd.get_dummies(features_set,prefix=['flower'])
prediction_set = pd.get_dummies(prediction_set,prefix=['flower'])

# Divide data in training and testing set
x_train, x_test, y_train, y_test = train_test_split(features_set[FEATURES], prediction_set, test_size = 0.3, random_state=42)

# Convert all data to float32 (This is needed to avoid type errors float64 vs float32)
x_train = x_train.astype(np.float32)
x_test = x_test.astype(np.float32)
y_train = y_train.astype(np.float32)
y_test = y_test.astype(np.float32)

# ----- Neural Network Tensorflow Definition -----
# Parameters
learning_rate = 0.001
epochs = 10000
batch_size = 10
display_step = 10

# NN Parameters
num_inputs = 4
n_hidden_1 = 32 # 1st hidden layer
num_classes = 3 # Ouput layer with 3 classes


# tf Graph input and output
X = tf.placeholder("float32", [None, num_inputs])
Y = tf.placeholder("float32", [None, num_classes])


# Store layers weights & biases
weights = {
    'h1': tf.Variable(tf.random_normal([num_inputs, n_hidden_1])),
    'out': tf.Variable(tf.random_normal([n_hidden_1, num_classes]))
}
biases = {
    'b1': tf.Variable(tf.random_normal([n_hidden_1])),
    'out': tf.Variable(tf.random_normal([num_classes]))
}


# --- Create the NN model ---
def neural_net(x):
    # Hidden fully connected layer
    layer_1 = tf.nn.relu(tf.add(tf.matmul(x, weights['h1']), biases['b1']))
    # Ouput fully connected layer with a neuron in each class
    out_layer = tf.matmul(layer_1, weights['out']) + biases['out']

    return out_layer


# --- Construct Model ---
model_scores = neural_net(X)

# Define loss and optimizer
# tf.nn.sigmoid_cross_entropy_with_logits (multilabel classification)
# tf.nn.softmax_cross_entropy_with_logits (multiclass classification)
pred = tf.nn.softmax_cross_entropy_with_logits(logits=model_scores, labels=Y)
loss_op = tf.reduce_mean(pred)

#optimizer = tf.train.AdagradOptimizer(learning_rate = learning_rate)
#optimizer = tf.train.GradientDescentOptimizer(learning_rate = learning_rate)
optimizer = tf.train.AdamOptimizer(learning_rate = learning_rate)
train_op = optimizer.minimize(loss_op)


# Predictions made by neural network while training
train_prediction = tf.nn.softmax(model_scores)

# Get the test values predicted by the neural network and the real test values
test_prediction = tf.nn.softmax(neural_net(x_test))
test_labels = y_test.values     # convert dataframe to numpy



# ----- Helper Functions -----
# Function Name:  accuracy
# Description: Evaluates the accuracy of the model
# Arguments: predictions - values given by trained NN
#           labels - real values to check on
def accuracy(predictions, labels):
    # print(np.argmax(predictions,1))
    # print(np.argmax(labels,1))
    preds_correct_boolean = np.argmax(predictions,1) == np.argmax(labels,1)
    correct_predictions = np.sum(preds_correct_boolean)
    accuracy = 100.0 * correct_predictions/predictions.shape[0]
    return accuracy


# ----- Start Training the Model (Main TF Execution) -----
# Initialize the variables (i.e. assign their default value)
init = tf.global_variables_initializer()

# ----- Start Training -----
with tf.Session() as sess:

    # Run initializer
    sess.run(init)

    # Run for each epoch
    for step in range(epochs+1):
        # Run optimization operation (backpropagation)
        _, loss, predictions = sess.run([train_op, loss_op, train_prediction], feed_dict={X: x_train, Y: y_train})

        if step % display_step == 0:
            print("Epoch " + str(step) + ", Epoch Loss= " + "{:.4f}".format(loss))

    print("Training Finished!")

    # The test_prediction.eval() evaluates the test_prediction data into the trained neural network
    print ("Test accuracy in %: " "{:.4f}".format(accuracy(test_prediction.eval(), test_labels)))
